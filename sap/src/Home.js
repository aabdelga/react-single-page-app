import React, { Component } from "react";
 
class Home extends Component {
  render() {
    return (
      <main className=".inner .cover">
        <div className=".opaque">
        <h1 className=".cover-heading">Welcome to Animal Safeguard!</h1>
        <p className=".lead">Our mission is to inform you about threatened and endangered animals. This
        website will give you vital information about what animals are at risk of extinction, where they
        can be found, and what their ecosystems are like. Our hope is that you use this information to
        make yourself aware of the threat to animals all around the world and to find ways to contribute
        to conservation efforts that will allow these animals to thrive for generations to come.</p>
        </div>
        <p className=".lead">
        <a href="/animals" className="btn btn-lg btn-secondary">Start exploring</a>
        </p>
      </main>
    );
  }
}
 
export default Home;