import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
  } from "react-router-dom";
  import Home from "./Home";
  import Animals from "./Animals";
  import Countries from "./Countries";
  import Ecosystems from "./Ecosystems";
  import About from "./About";
 
class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div className=".cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
          <header className=".masthead mb-auto">
          <div className=".inner">
            <h3 className=".masthead-brand">Animal Safeguard</h3>
            <nav className="nav nav-masthead justify-content-center">
              <NavLink className="nav-link" exact to="/">Home</NavLink>
              <NavLink className="nav-link" to="/animals">Animals</NavLink>
              <NavLink className="nav-link" to="/countries">Countries</NavLink>
              <NavLink className="nav-link" to="/ecosystems">Ecosystems</NavLink>
              <NavLink className="nav-link" to="/about">About</NavLink>
            </nav>
          </div>
          </header>

          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route path="/animals" component={Animals}/>
            <Route path="/countries" component={Countries}/>
            <Route path="/ecosystems" component={Ecosystems}/>
            <Route path="/about" component={About}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}
 
export default Main;