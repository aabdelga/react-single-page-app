import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import "./cover.css";
import "./animals.css";
 
ReactDOM.render(
  <Main/>, 
  document.getElementById("root")
);