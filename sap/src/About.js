import React from 'react';

class About extends React.Component{
    render() {
        return (
            <main role="main" className="inner cover">
            <div className="opaque">
            <h1 className="cover-heading">About Us</h1>
            <p className="lead">We are the Animal Safeguard team! Here you will find information about the developers of this website and their
            contributions to the website.</p>
            </div>
            </main>
        );
    }
}

export default About;