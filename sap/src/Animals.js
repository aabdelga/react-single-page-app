import React from 'react';

class Animals extends React.Component{
    render() {
        return(
            <main role="main" className="inner cover">
            <div className=".opaque">
                <h1 className="cover-heading">Animals</h1>
                <p className="lead">Here you will find information about different species,
                such as their scientific name, habitat, estimated population, and
                threat level.</p>
            </div>
            </main>
        );
    }
}

export default Animals;